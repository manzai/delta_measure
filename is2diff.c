// is2diff.c 

// Convert an uint32 array of increasing values a[0..n-1] into an
// array of positive increments s[0..n-1] with s[i] = a[i+1]-a[i]+2
// and s[n-1] = 1 (so s[n-1] is different from all previous entries) 
// The input file is assumed to be in a binary file whose
// initial skip bytes are ignored (default skip=0)
// the output is also stored in a binary file as a sequence of uint32

// Exactly the same conversion done in this tool can be done inside delta32,
// with option -d so this tool is useful if you want to compute some 
// other complexity measure for the difference sequence

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <inttypes.h>

static void die(const char *s)
{
    perror(s);
    exit(EXIT_FAILURE);
}


// read a set of uint32's from a binary file skipping the first skip bytes 
static uint32_t *read_ints(char *name, long *np, int skip)
{
  FILE *f = fopen(name,"rb");
  if (f==NULL) die("Cannot open input file");

  // get file length 
  if(fseek(f,0,SEEK_END)!=0) die("fseek error");
  fprintf(stderr,"Input consists of %zd bytes, skipping intial %d bytes\n",ftell(f),skip);
  long nn = ftell(f) - skip;
  // check input is valid
  if(nn<=0) die("No input data");
  if(nn%4!=0) die("Input size not multiple of 4");
  long n = nn/4;
  // alloc
  uint32_t *t = malloc(n*sizeof(uint32_t));
  if(t==NULL) die("Cannot malloc");
  // read file skipping initial bytes
  fseek(f,skip,SEEK_SET);
  assert(sizeof(*t)==4);
  size_t s = fread(t,sizeof(*t),n,f);
  if(s!=n) die("Read error");
  if(fclose(f)!=0) die("File close error");
  // return
  *np = n;
  return t;
}



int main(int argc, char *argv[])
{
  char *fin, *fout;     // input and output file names
  uint32_t *a;          // array of symbols 
  long n;               // length of a[]
  extern int optind, opterr, optopt;

  /* ------------- read options from command line ----------- */
  opterr = 0;
  int c, skip=0;       // default skip
  while ((c=getopt(argc, argv, "s:")) != -1) {
    switch (c) 
      {
      case 's':
        skip=atoi(optarg); break;
      case '?':
        fprintf(stderr,"Unknown option: %c\n", optopt);
        exit(1);
      }
  }
  if(skip<0) {
    fprintf(stderr,"Number of skipped bytes %d must be non negative\n",skip);
    exit(EXIT_FAILURE); 
  }

  if(optind==argc-2) {
    fin=argv[optind];
    fout=argv[optind+1];
  }
  else {
    fprintf(stderr, "Usage:  %s [-s b] [-d] filename\n",argv[0]);
    fprintf(stderr,"\t-s b       initial bytes to skip, def none\n");    
    fprintf(stderr,"\t-d         compute delta for the difference array a[i+1]-a[i]\n\n");    
    return 1;
  }
  
  // start measuring wall clock time 
  time_t start_wc = time(NULL);
  // read input file file
  a = read_ints(fin,&n,skip);
  // trasform input sequence
  //   compute differences +2 (we do not want 0 or 1)
  //   compute largest computed symbol k
  uint32_t old = a[0];
  long k=0;
  long delta0 = 0;      // number of consecutive equal input symbols  
  for(long i=1;i<n;i++) {
    if(a[i]<old) {
      fprintf(stderr,"Decreasing pair: %ld: %d vs %ld: %d\n",i-1,old,i,a[i]);
      exit(EXIT_FAILURE);  
    } 
    else if(a[i]==old) delta0++; 
    a[i-1] = (a[i]-old)+2; // smallest new entry is 2
    if(a[i-1]>k) k = a[i-1]; 
    old = a[i];
  }
  assert(old==a[n-1]);
  a[n-1]=1;  // insert a symbol different from all the previous ones. 
  size_t outint = n;


  // report
  fprintf(stderr,"Number of input integers: %ld\n", n);
  fprintf(stderr,"Largest input integer: %d\n", old);
  fprintf(stderr,"Number of output differences: %zd\n", outint);
  fprintf(stderr,"Largest output difference: %ld\n", k);
  fprintf(stderr,"Number of repeated input values: %ld\n",delta0);
  
  // write
  FILE *f = fopen(fout,"wb");
  if(f==NULL) die("Cannot open output file");
  size_t s = fwrite(a,sizeof(int32_t), outint, f);
  if(s!=outint) die("Write error");
  fclose(f);
  fprintf(stderr,"====== Elapsed time: %.0lf wall clock seconds\n", difftime(time(NULL),start_wc));
  return 0;
}
