// tool to compute the delta compressibility measure of a sequence of int32's
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <inttypes.h>
#include "gsa/gsacak.h"

static void die(const char *s)
{
    perror(s);
    exit(EXIT_FAILURE);
}

// additional uint32 entries allocated with the main array
// used to store the EOS/EOF pair 
#define EXTRA_INTS 2

// read a set of uint32's from a binary file skipping the first skip bytes 
static uint32_t *read_ints(char *name, long *np, int skip)
{
  FILE *f = fopen(name,"rb");
  if (f==NULL) die("Cannot open input file");

  // get file length 
  if(fseek(f,0,SEEK_END)!=0) die("fseek error");
  fprintf(stderr,"Input consists of %zd bytes, skipping intial %d bytes\n",ftell(f),skip);
  long nn = ftell(f) - skip;
  // check input is valid
  if(nn<=0) die("No input data");
  if(nn%4!=0) die("Input size not multiple of 4");
  long n = nn/4;
  // alloc including extra space
  uint32_t *t = malloc((n+EXTRA_INTS)*sizeof(uint32_t));
  if(t==NULL) die("Malloc error");
  // read file skipping initial bytes
  fseek(f,skip,SEEK_SET);
  assert(sizeof(*t)==4);
  size_t s = fread(t,sizeof(*t),n,f);
  if(s!=n) die("File read error");
  if(fclose(f)!=0) die("File close error");
  // return
  *np = n;
  return t;
}


int main(int argc, char *argv[])
{
  char *fnam;     // input file name
  uint32_t *a;    // array of symbols 
  long n;         // length of input sequence as read from file
  uint32_t m;     // lenght of sequence to process
  extern int optind, opterr, optopt;

  /* ------------- read options from command line ----------- */
  opterr = 0;
  int c, skip=0;              // default skip
  bool compute_diff = false;  // do not compute differences a[i+1]-a[i] by default
  int verbose = 0;
  while ((c=getopt(argc, argv, "s:dv")) != -1) {
    switch (c) 
      {
      case 's':
        skip=atoi(optarg); break;
      case 'd':
        compute_diff = true;  break;
      case 'v':
        verbose = 1;  break;
      case '?':
        fprintf(stderr,"Unknown option: %c\n", optopt);
        exit(1);
      }
  }
  if(skip<0) {
    fprintf(stderr,"Number of skipped bytes %d must be non negative\n",skip);
    exit(EXIT_FAILURE); 
  }
 
 if(optind==argc-1)
    fnam=argv[optind];
  else {
    fprintf(stderr, "Usage:  %s filename\n",argv[0]);
    fprintf(stderr,"\t-s b       initial bytes to skip, def none\n");    
    fprintf(stderr,"\t-d         compute delta for the difference array a[i+1]-a[i]\n");    
    fprintf(stderr,"\t-v         verbose output on stderr\n");    
    return 1;
  }

  // start measuring wall clock time and read input file
  time_t start_wc = time(NULL);
  a = read_ints(fnam,&n,skip); 
  
  // transform input sequence:
  //   compute differences a[i+1]-a[i]+2 or increase each entry a[i] by 2
  //   compute largest symbol maxchar
  //   add a final eos+eof pair <1, 0> (required by gsaka_lcp)  

  uint32_t maxchar=0;            // largest symbol 
  for(long i=0;i<n-1;i++) {
    if(compute_diff) {           // the actual input is the difference a[i+1]-a[i]
      if(a[i+1]<a[i]) {
        fprintf(stderr,"Decreasing pair: %ld: %d vs %ld: %d\n",i,a[i],i+1,a[i+1]);
        exit(EXIT_FAILURE);  
      }
      a[i] = a[i+1]-a[i]+2; // make all differences >1 
    }
    else { // a[i]+2 is the actual input (0 and 1 are reserved)
      a[i] += 2;  
    }
    // compute largest symbol 
    if(a[i]>maxchar) maxchar=a[i];
  }
  // we are left with the last symbol a[n-1], and the trailing eos/eof 
  if(compute_diff) {
    a[n-1]=1; a[n]=0; n=n+1;
  }
  else {
    a[n-1] +=2 ; a[n]=1; a[n+1]=0; n=n+2;
  }
    
  // report so far 
  if(verbose) {
    fprintf(stderr,"Number of integers in sequence: %ld\n", n-2);
    fprintf(stderr,"Largest symbol: %u\n", maxchar);
    fprintf(stderr,"Elapsed time: %.0lf wall clock seconds\n",difftime(time(NULL),start_wc));
  }  
  // ------------ virtually build the s array
  if(n>=0x7fffffff) {
    fprintf(stderr,"sa/lcp sequence containing more than 2^31-1 ints\n"); 
    exit(EXIT_FAILURE);
  }
  else m = n;
    
  // ------------ compute sa[] and lcp[] for the array s[]  
  uint32_t *sa = malloc(m*sizeof(*sa));
  if(sa==NULL) die("malloc failed");
  int32_t *lcp = malloc(m*sizeof(*lcp));
  if(lcp==NULL) die("malloc failed");
  // compute sa and lcp
  int d = gsacak_int(a, sa, lcp, NULL, m, maxchar+1);
  if(d<0) {fprintf(stderr,"Error computing sa/lcp\n"); exit(EXIT_FAILURE);}
  else fprintf(stderr,"sa/lcp computed. Elapsed time: %.0lf wall clock seconds\n",difftime(time(NULL),start_wc));
  // the first two sa entry are eof and eos
  assert(sa[0]==m-1 && sa[1] == m-2);
  // the first three lcp must be 0; note lcp[i] = LCP(sa[i-1],sa[i])
  assert(lcp[0]==0 && lcp[1]==0 && lcp[2]==0);
  
  // --------------- compute u[]
  // for i=1..m-2 theoretically u[i] should contain the number of unique 
  // substrings of length i (up to m-2 since we are not considering the
  // eos and eof symbols). However, we use an implicit representation
  // For j=2...m-1 we consider the substring starting at sa[j],
  // this has length lenj = m-2-sa[j]. Then, all prefixes of length
  // lcp[j]+1, lcp[j]+2, ..., lenj are unique. Instead of incrementing by 1
  // all u[]'s  entries in that range we increment u[lenj] by 1
  // and decrement u[lcp[j]] by 1 and use the convention that a value in
  // u[z] should be reported also in u[z-1], u[z-2], ... u[1]
  // positive and negative values nicely cancel out and the overal cost is linear 
  int32_t *u = (int32_t *) a;    // store u[] replacing a[]
  memset(u,0,(m-1)*sizeof(int32_t));
  // the following is a dumb implementation of the procedure outlined above
  // there is no need to scan sa[] since m-2-sa[j] clearly span all the values
  // between 1 and m-2 so we could just initialize u[]=1 or better keep track
  // of these 1s in the compuation below. I am keeping to dumb version
  // just to double check computations done in a different way   
  for(long j=2;j<m;j++) {
    long lenj = m-2 - sa[j];
    assert(lenj>0 && lenj <= m-2);
    assert(lcp[j]>=0 && lcp[j]<lenj);// it is not lcp[j]==lenj since eos<alpha
    u[lenj]+=1; u[lcp[j]] -=1;
  }
  assert(u[m-2]==1);

  // ------- compute delta 
  double tmp, delta = 0;
  long deltaj = 0;
  long sj=0;
  for(long j=m-2;j>0;j--) {
    sj += u[j];
    assert(sj>0);
    tmp = (double) sj/j;
    if(tmp>delta) {
      delta=tmp; deltaj = j;
      if(verbose) fprintf(stderr,"  k=%ld, S(k)=%ld, delta=%g\n",j,sj,delta);
    }
  }
  assert(sj+u[0]==0);

  // --- deallocate
  free(lcp); free(sa); free(a);
  fprintf(stderr,"Elapsed time: %.0lf wall clock seconds\n", difftime(time(NULL),start_wc));
  
  // --- report delta 
  fprintf(stdout,"=== file: %s   delta: %f [%ld]\n",fnam,delta,deltaj);
  
  return 0;
} 
