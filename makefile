# compiler and compilation flags
CC=gcc
CFLAGS=-g -Wall -O3 -std=gnu99
LDLIBS=-lm

EXECS=delta bigdelta vbigdelta delta32 is2diff
 
all: $(EXECS)

delta: delta.c sa64/divsufsort64.a
	$(CC) $(CFLAGS) -o $@ $^

bigdelta: bigdelta.c sa64/divsufsort64.a
	$(CC) $(CFLAGS) -o $@ $^

vbigdelta: bigdelta.c sa64/divsufsort64.a
	$(CC) $(CFLAGS) -o $@ $^ -DVERY_BIG

delta32: delta32.c gsa/gsacak.o 
	$(CC) $(CFLAGS) -o $@ $^

gsa/gsacak.o: gsa/gsacak.c gsa/gsacak.h
	$(CC) $(CFLAGS) -c -o $@ $<

sa64/divsufsort64.a:
	make -C sa64

clean: 
	rm -f $(EXEC)
	make -C sa64 clean
	
