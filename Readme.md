# Tools for computing the delta measure of repetitiveness

For a definition of the *delta* measure and its relationship with other repetitiveness measures see the survey [Indexing Highly Repetitive String Collections, Part I: Repetitiveness Measures](https://dl.acm.org/doi/10.1145/3434399) by Gonzalo Navarro.

The computation of the delta measure is done using the LCP array and the observation that given a sequence $T[1,n]$ the number of distint substring of length $k$ is given by $n-k+1 -|\{ j \mid LCP[j] \geq k \}|$.


## Installation 

To compile all tools just issue the command `make`. All tools invoked without arguments provide basic usage instructions. 
 

## delta

Tool to compute the delta measure for a sequence of uint8's of length at most $2^{32}-2$. Running time is O(n log n) because of the use of [libdivsufsort](https://github.com/y-256/libdivsufsort) by Yuta Mori for the computation of the SA (the best linear time suffix sorting algorithms SAIS and SACA require a unique symbol at the end, moreover libdivsufsort is usually faster in practice). Space usage is 9n bytes: n bytes for the input sequence, and 4n bytes each for its SA and LCP array.


## bigdelta

Tool to compute the delta measure for a sequence of uint8's of length at most $2^{40}-2$. The only difference with *delta* is that each SA and LCP entry uses 5 bytes, so the overall space usage is 11n bytes.


## vbigdelta

Tool to compute the delta measure for a sequence of uint8's of length at most $2^{48}-2$. Uses the same source code as *bigdelta* with the only difference that each SA and LCP entry uses 6 bytes, so the overall space usage is 13n bytes.



## delta32

Tool to compute the delta measure for a sequence of positive 32-bit integers. Running time is linear (dominated by the computation of SA and LCP arrays done using [gSACA-K+LCP](https://github.com/felipelouza/gsa-is)), space usage is *12n* bytes where *n* is the number of ints in the input sequence. Note: the use of SACA-K is forced by the potentially very large alphabet.

With option *-d* the tool computes the delta measure of the difference of consecutive input values (which must be non-decreasing). Option *-s* can be used to skip (ignore) an arbitrary number of leading *bytes* in the input file. 

Current limitation: number of input integers can be at most $2^{31}-3$. Can be easily removed using the 64 bit version of gSACA-K+LCP but the total space usage would rise to *20n* bytes. 


## is2diff

Tool to convert an array of increasing values *a[0..n-1]* into an
array of positive increments *s[0..n-1]* with *s[i] = a[i+1]-a[i]+2*
and *s[n-1] = 1*. 

The input file is assumed to be a binary file containing *uint32*, whose initial *skip* bytes (not ints!) are ignored (default *skip=0*). The output array *s[0..n-1]* is stored to a binary file as a sequence of *uint32*.

The output file, with a final value not appearing elsewhere, can be given as input to the tool *h0_lz77* from the [xxsds/DYNAMIC](https://github.com/xxsds/DYNAMIC) repository to compute the LZ77 parsing of the difference sequence.


