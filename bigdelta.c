/* *********************************************************************
   Compute the delta measure for a sequence of up to 1TB (256TB) chars

   The computation is done as in delta.c except that the sa[j] and lcp[j]
   are assumed to take up to 40 bits and are stored together in 64+16 bits
   
   Compiling with -DVERY_BIG uses 48 bits per entry so sa[j] and lcp[j]
   are stored together in 64+32 bits

   Note that immediately after the call sa2lcp_9n(T,n,SA-1,LCP-1) 
   we have computed and stored the SA and LCP array and you can replace 
   the computation of delta with whatever computation you are interested in
   
   Note: valgrind gives a warning
     Conditional jump or move depends on uninitialised value(s)
   at the line assert(nextk==0); at the end of sa2lcp_9n() function
   and (many) warnings at the line SA[i] &= ~MASKSA in main() when 
   compiled with the -O option. However, the results coincdes with those
   of delta.c that uses the same algorithm and runs fine on valgrind.
  *********************************************************************** */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include "sa64/divsufsort64.h"



#define ALPHABET_SIZE 256

#ifndef VERY_BIG
// 5 bytes per entry version
typedef uint16_t ulcpL_t;   
#define LOW_BITS 16
#define ALL_BITS 40
#define HIGH_BITS 24
#define MASKSA 0xFFFFFFFFFFL     // used to read a SA value 
#define MASKHIGH 0xFFFFFF0000L   // used to read the lcp high bits
#else
// 6 bytes per entry version 
typedef uint32_t ulcpL_t; 
#define LOW_BITS 32
#define ALL_BITS 48
#define HIGH_BITS 16
#define MASKSA 0xFFFFFFFFFFFFL
#define MASKHIGH 0xFFFF00000000L
#endif


// vebosity level
int Verbose;

// local prototypes
static void print_help(const char *progname, int status);
static void sa2lcp_9n(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *lcp);
saidx64_t sa2ranknext(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *lcp);
static void check_lcp_array(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *lcp);



int main(int argc, char *argv[]) {
  extern char *optarg;
  extern int optind, opterr, optopt;
  int32_t c, lcp, check_sa, check_lcp;
  FILE *fp;
  const char *fname;
  sauchar_t *T;
  saidx64_t *SA;
  ulcpL_t *LCP;
  saidx64_t n;
  clock_t start0, start, finish;
  
  start0 = clock();
  Verbose=lcp=check_sa=check_lcp=0;  // default values
  while ((c=getopt(argc, argv, "vck")) != -1) {
    switch (c) 
      {
      case 'v':
        Verbose++; break;
      case 'c':
        check_sa=1; break;
      case 'k':
        check_lcp=1; break;        
      case '?':
        print_help(argv[0],EXIT_FAILURE);;
      }
  }
  if(optind+1==argc) {
    fname=argv[optind];
  }
  else  print_help(argv[0],EXIT_FAILURE);


  /* Open a file for reading. */
  if((fp = fopen(fname, "rb")) == NULL) {
    fprintf(stderr, "%s: Cannot open file `%s': ", argv[0], fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }

  /* Get the file size. */
  if(fseek(fp, 0, SEEK_END) == 0) {
    off_t nx = ftello(fp);
    rewind(fp);
    if(nx < 0) {
      fprintf(stderr, "%s: Cannot ftell `%s': ", argv[0], fname);
      perror(NULL);
      exit(EXIT_FAILURE);
    }
    else n = (saidx64_t) nx;
  } 
  else {
    fprintf(stderr, "%s: Cannot fseek `%s': ", argv[0], fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }
  assert(n>0);
  if(n>MASKSA) {
    fprintf(stderr, "%s: input file %s is too big: max size is 2^%d-1\n", argv[0],fname,ALL_BITS);
    exit(EXIT_FAILURE);
  }

  /* Allocate 11n (or 13n  bytes of memory. */
  SA = (saidx64_t *)malloc((size_t)n * sizeof(saidx64_t));
  LCP = (ulcpL_t *)malloc((size_t)n * sizeof(ulcpL_t));
  T = (sauchar_t *)malloc((size_t)n * sizeof(sauchar_t));
  // if using SA-1 later on is a problem use something like:
  // saidx64_t *sa1 = (saidx64_t *)malloc((size_t)n * sizeof(saidx64_t)+1);
  // SA = sa1+1;
  if((T == NULL) || (SA == NULL) || (LCP==NULL)) {
    fprintf(stderr, "%s: Cannot allocate memory.\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Read n bytes of data. */
  if(fread(T, sizeof(sauchar_t), (size_t)n, fp) != (size_t)n) {
    fprintf(stderr, "%s: %s `%s': ",
      argv[0],
      (ferror(fp) || !feof(fp)) ? "Cannot read from" : "Unexpected EOF in",
      fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }
  fclose(fp);

  /* Construct the suffix array. */
  if(Verbose)
    fprintf(stderr, "%s: %"PRIdSAIDX64_T" bytes ... ", fname, n);
  start = clock();
  if(divsufsort64(T, SA, (saidx64_t)n) != 0) {
    fprintf(stderr, "%s: Cannot allocate memory.\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  finish = clock();
  if(Verbose)
    fprintf(stderr, "%.4f sec\n", (double)(finish - start) / (double)CLOCKS_PER_SEC);

  /* Check the suffix array. */
  if(check_sa)
    if(sufcheck64(T, SA, n, Verbose) != 0) { exit(EXIT_FAILURE); }

  // compute the lcp
  start = clock();
  if(Verbose) fprintf(stderr, "Computing LCP values... ");
  sa2lcp_9n(T,n,SA-1,LCP-1);
  finish = clock();
  if(Verbose)
    fprintf(stderr, "%.4f sec\n", (double)(finish - start) / (double)CLOCKS_PER_SEC);
  if(check_lcp) {  
    check_lcp_array(T,n,SA,LCP); // very slow!!!! replace with a faster one
  }
  // note: here we have availble the SA and LCP array in 5+5 (or 6+6) format
  // not really tested but they can be used for other applications 

  // now we discard the SA overwriting it with lcpstat
  // lcpstat[j] = |{i | lcp[i]==j}|
  for(saidx64_t i=0;i<n;i++) 
    SA[i] &= ~MASKSA;   // set SA bits to 0 
    
  saidx64_t *lcpstats = SA;  
  saidx64_t maxlcp = ((SA[0]>>HIGH_BITS)&MASKHIGH)|LCP[0]; 
  for(saidx64_t i=1;i<n;i++) {
    saidx64_t lcpi =  ((SA[i]>>HIGH_BITS)&MASKHIGH)|LCP[i];     
    assert(lcpi<=maxlcp); 
    lcpstats[lcpi]++;      // the lcp high bits are not modified  
  }    

  // now we compute delta using the formula
  //  S(k) = n-k+1 - \sum_{i=k}^{n-1} lcpstat[i] 
  // first candidate is k=maxlcp+1
  saidx64_t kmax = maxlcp + 1;
  assert(kmax <= n);
  double sk = n-kmax+1;
  double delta = sk/kmax;
  saidx64_t sumlcpstat = 0;
  for(saidx64_t k=kmax-1;k>0;k--) {
    sumlcpstat += lcpstats[k]&MASKSA;
    sk = n-k+1 - sumlcpstat;
    assert(sk>0);
    double tmp = sk/k;
    if(tmp>delta) {
      delta=tmp; kmax=k;
      if(Verbose) fprintf(stderr,"  k=%ld, S(k)=%ld, delta=%g\n",kmax,(long)sk,delta);
    }
  }
  finish = clock();
  fprintf(stderr, "Total elapsed time: %.4f sec\n", (double)(finish - start0) / (double)CLOCKS_PER_SEC);  
  // --- report delta 
  fprintf(stdout,"=== file: %s   delta: %lf [%ld]\n",fname,delta,kmax);

  /* Deallocate memory. */
  free(T);
  free(LCP);
  free(SA);

  return 0;
}



/* ***********************************************************************
   WARNING sa, lcp, rank_next are 1 based: sa[n] is ok sa[0] is segfault  
   space economical computation of the lcp array
   This procedure is similar to the algorithm of kasai et al.  but we compute 
   the rank of i using the rank_next map (stored in the rn array) instead of
   the rank array. The advantage is that as soon as we have read rn[k] that
   position is no longer needed and we can use it to store the lcp.
   Thus, rn[] and lcp[] share the same memory.
   It is assumed that n < 2^40 (2^48 if VERY_BIG is defined) so each 
   sa/lcp/rn entry is stored in 5 (6) bytes 
   The overall space requirements of the procedure is
      n + 5n (sa) + 5n (lcp/rn) = 11n
      (or n + 6n (sa) + 6n (lcp/rn) = 13n if VERY_BIG)
   The name 9n is historical and denotes the algorithm as defined in the SWAT 04 paper 
    input
      t[0,n-1] input text 
      sa[1,n]  suffix array each entry taking at most 40 bits, the remaining 24 bits are 0
      lcp[1,n] uninitialized array of uint16 (or uint32 if VERY_BIG)
    return
      nothing
      the lcp[2,n] entries are stored in the high 24 bit of each sa[] entry
                   and in the 16 bit of each lcp[] entry 
                   sa[j][40-63] contains bits 16-39, while lcp[j] bits 0-15
      lcp[i] is lcp between t[sa[i-1]...] and t[sa[i]...]
    no additional space in addition to t[], sa[], lcp[] is used 
   *********************************************************************** */
static void sa2lcp_9n(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *lcp)
{
  saidx64_t i,h,j,k,nextk=-1;
  ulcpL_t *rn;
  saidx64_t maxlcp = 0;
  
  // rn and lcp are pointers to the same array
  rn =  lcp;
  // compute rank_next map
  k = sa2ranknext(t, n, sa, rn); // k is the rank of t[0...]
  if(Verbose>1)
    fprintf(stderr,"ranknext computation completed\n");
  for(h=i=0; i<n; i++,k=nextk) {
    assert(k>0 && i==(sa[k]&MASKSA)); 
    nextk=((sa[k]>>HIGH_BITS)&MASKHIGH)|rn[k];     // read nextk before it is overwritten
    if(k>1) {
      // compute lcp between suffixes of rank k and k-1 (recall i==sa[k])
      j = sa[k-1] & MASKSA;
      while(i+h<n && j+h<n && t[i+h]==t[j+h])
        h++;
      // check lcp fits in 40/48 bits   
      if(h>MASKSA) {
        fprintf(stderr,">%d bits lcp value: %ld. Exiting\n",ALL_BITS, (long) h); 
        exit(EXIT_FAILURE);
      }
      if(h>maxlcp) maxlcp=h;           // compute also maximum LCP 
      lcp[k]=h;        // save bits 0:15
      sa[k] = (h>>LOW_BITS)<<ALL_BITS | (sa[k]&MASKSA); // save bits 16:39   
    }
    if(h>0) h--;
  }
  assert(nextk==0);        // we have reached the last suffix s[n-1]
  lcp[1] = maxlcp;         // write maxlcp to lcp[1]
  sa[1] = ((maxlcp>>LOW_BITS)<<ALL_BITS) | (sa[1]&MASKSA);
  return;
}


/* *******************************************************************
   WARNING sa, lcp, rank_next are 1 based 
   input 
     n size of text
     t[0,n-1] input text
     sa[1,n] suffix array (low 32 bits)
  output
    rank_next[1,n] is written to rn[1,n] (32 bits) and in the high 16 bits of sa[1,n] 
    note: ranks are in the range [1,n]
  return 
    r0 = rank of t[0,n-1] (it is not a rank_next value), 
    sa[r0]=0 (this is an invalid  rank)
  space: inplace except for the occ and count arrays (occ can be avoided)
  ******************************************************************* */
saidx64_t sa2ranknext(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *rn)
{
  saidx64_t i,j,sai,c,r0=0;
  saidx64_t count[ALPHABET_SIZE], occ[ALPHABET_SIZE];

  // compute # occurrences of each char 
  for(i=0;i<ALPHABET_SIZE;i++) occ[i]=0;
  for(i=0;i<n;i++) occ[t[i]]++;
  // --- occ -> count
  count[0]=0;
  for(i=1;i<ALPHABET_SIZE;i++) {
    count[i] = count[i-1] + occ[i-1];
  }
  
  // --- sa+t -> rank_next
  j = ++count[t[n-1]];       // this is bwt[0]
  assert(j>0 && j<=n);
  sa[j] |= 0; // here there was rank_next[j]=0, note 0 is an invalid rank value;
  for(i=1;i<=n;i++) {
    sai = (sa[i] & MASKSA);
    assert(sai>=0 && sai<n);
    if(sai == 0)
      r0 = i;
    else {
      c = t[sai-1];
      j = ++count[c];  // rank_next[j] = i
      assert(j>0 && j<=n); 
      rn[j] = i;                // save low bits 0..15
      if(i>>LOW_BITS)           // is there any high bit set?
        sa[j] |= ((i>>LOW_BITS)<<ALL_BITS); // save bits 16..39 of i in sa[j][40..63]
    }
  }
  assert(r0>0);
  return r0;
}


/* *******************************************************************
   check the correctness of a lcp array with a (very slow) 
   character by character comparison of consecutive suffixes
   input
     t[0,n-1]   input text 
     sa[0,n-1],lcp[1,n-1]  sa and lcp in 5+5 or 6+6 bytes format
   ******************************************************************* */ 
static void check_lcp_array(sauchar_t *t, saidx64_t n, saidx64_t *sa, ulcpL_t *lcp)
{
  saidx64_t i,j,lcpi,k,h;

  if(Verbose)
    fprintf(stderr,"Checking lcp values (very slow)...\n");
  for(i=1;i<n;i++) {
    j = (sa[i-1] & MASKSA); 
    k=(sa[i] & MASKSA);
    for(h=0;j+h<n && k+h<n;h++)
      if(t[j+h]!=t[k+h]) break;
    lcpi = ((sa[i]>>HIGH_BITS)& MASKHIGH)|lcp[i];   
    if(lcpi!=h) {
      fprintf(stderr,"FATAL ERROR! Incorrect LCP value: lcp[%"PRIdSAIDX64_T
                     "]=%"PRIdSAIDX64_T"!=%"PRIdSAIDX64_T"\n",
        i,lcpi,h);
      exit(EXIT_FAILURE);
    }
  }
  if(Verbose)
    fprintf(stderr,"LCP array OK!\n");
}


static void
print_help(const char *progname, int status) {
  fprintf(stderr, "usage: %s filename\n", progname);
  fprintf(stderr, "\t-c check sa\n");
  fprintf(stderr, "\t-k check lcp (very slow!)\n");
  fprintf(stderr, "\t-v verbose output\n\n");
  exit(status);
}


