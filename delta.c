/* *********************************************************************
   Compute the delta measure for a sequence of up to 4GB chars
    
   Sa construction is done using libdivsufsort64 by Yuta Mori
     https://github.com/y-256/libdivsufsort
   Lcp construction is done using the swat04 9n algo
   Space usage is n bytes for the text, 8n bytes for SA and LCP array
   which are stored in the same array of 64 bit ints
   Speed could be improved by computing the plcp instead of the lcp array
   
   The formula used for the computation of the number S(k) of distinct 
   substrings of length k is
     S(k) = n-k+1 - |{j | lcp[j]>=k}|    
       
  *********************************************************************** */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include "sa64/divsufsort64.h"


// define alphabet size based on the size of sauchar
#define ALPHABET_SIZE 256
#define MASK32 0xFFFFFFFF


// vebosity level
int Verbose;

// local prototypes
static void print_help(const char *progname, int status);
static void sa2lcp_9n(sauchar_t *t, saidx64_t n, saidx64_t *sa);
saidx64_t sa2ranknext(sauchar_t *t, saidx64_t n, saidx64_t *sa);
static void check_lcp_array(sauchar_t *t, saidx64_t n, saidx64_t *sa);



int main(int argc, char *argv[]) {
  extern char *optarg;
  extern int optind, opterr, optopt;
  int32_t c, check_sa, check_lcp;
  FILE *fp;
  const char *fname;
  sauchar_t *T;
  saidx64_t *SA;
  saidx64_t n;
  clock_t start, finish, start0;

  start0 = clock();
  Verbose=check_sa=check_lcp=0;  // default values
  while ((c=getopt(argc, argv, "vck")) != -1) {
    switch (c) 
      {
      case 'v':
        Verbose++; break;
      case 'c':
        check_sa=1; break;
      case 'k':
        check_lcp=1; break;        
      case '?':
        print_help(argv[0],EXIT_FAILURE);
      }
  }
  if(optind+1==argc) {
    fname=argv[optind];
  }
  else  print_help(argv[0],EXIT_FAILURE);


  /* Open a file for reading. */
  if((fp = fopen(fname, "rb")) == NULL) {
    fprintf(stderr, "%s: Cannot open file `%s': ", argv[0], fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }

  /* Get the file size. */
  if(fseek(fp, 0, SEEK_END) == 0) {
    off_t nx = ftello(fp);
    rewind(fp);
    if(nx < 0) {
      fprintf(stderr, "%s: Cannot ftell `%s': ", argv[0], fname);
      perror(NULL);
      exit(EXIT_FAILURE);
    }
    else n = (saidx64_t) nx;
  } 
  else {
    fprintf(stderr, "%s: Cannot fseek `%s': ", argv[0], fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }
  assert(n>0);
  if(n>=0xFFFFFFFF) {
    // not sure, maybe it works even for n=2^31 (we just need to represent n-1 in 32 bits)  
    fprintf(stderr, "%s: input file %s is too big: max size is 2^32-2\n", argv[0], fname);
    exit(EXIT_FAILURE);
  }

  /* Allocate 9 n bytes of memory. */
  SA = (saidx64_t *)malloc((size_t)n * sizeof(saidx64_t));
  T = (sauchar_t *)malloc((size_t)n * sizeof(sauchar_t));
  // if using SA-1 later on is a problem use something like:
  // saidx64_t *sa1 = (saidx64_t *)malloc((size_t)n * sizeof(saidx64_t)+1);
  // SA = sa1+1;
  if((T == NULL) || (SA == NULL)) {
    fprintf(stderr, "%s: Cannot allocate memory.\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Read n bytes of data. */
  if(fread(T, sizeof(sauchar_t), (size_t)n, fp) != (size_t)n) {
    fprintf(stderr, "%s: %s `%s': ",
      argv[0],
      (ferror(fp) || !feof(fp)) ? "Cannot read from" : "Unexpected EOF in",
      fname);
    perror(NULL);
    exit(EXIT_FAILURE);
  }
  fclose(fp);

  /* Construct the suffix array. */
  if(Verbose)
    fprintf(stderr, "%s: %"PRIdSAIDX64_T" bytes ... ", fname, n);
  start = clock();
  if(divsufsort64(T, SA, (saidx64_t)n) != 0) {
    fprintf(stderr, "%s: Cannot allocate memory.\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  finish = clock();
  if(Verbose)
    fprintf(stderr, "%.4f sec\n", (double)(finish - start) / (double)CLOCKS_PER_SEC);

  /* Check the suffix array. */
  if(check_sa)
    if(sufcheck64(T, SA, n, Verbose) != 0) { exit(EXIT_FAILURE); }

  start = clock();
  if(Verbose) fprintf(stderr, "Computing LCP values... ");
  sa2lcp_9n(T,n,SA-1);
  finish = clock();
  if(Verbose) {
    fprintf(stderr, "%.4f sec\n", (double)(finish - start) / (double)CLOCKS_PER_SEC);
    fprintf(stderr, "Maximum LCP: %ld\n", (SA[0]>>32)&MASK32);
  }
  if(check_lcp) {  
    check_lcp_array(T,n,SA); // very slow!!!! replace with a faster one
  }
  // save lcp in a "real" uint32_t array
  uint32_t *lcp = (uint32_t *) SA;
  for(saidx64_t i=0;i<n;i++) {
    lcp[i] = (SA[i]>>32);
  }   
  // use remaining part of SA for lcpstats
  uint32_t *lcpstats = lcp+n;
  memset(lcpstats,0,n*sizeof(*lcpstats)); // clear lcpstats
  for(saidx64_t i=1;i<n;i++) {
    assert(lcp[i]<=lcp[0]);  // lkcp[0] is maxlcp
    lcpstats[lcp[i]]++;      // lcpstat[j] = # lcp==j
  } 
  // now we compute delta using the formula
  //  S(k) = n-k+1 - \sum_{i=k}^{n-1} lcpstat[i] 
  // first candidate is k=maxlcp+1
  saidx64_t kmax = lcp[0] + 1L;
  assert(kmax <= n);
  double sk = n-kmax+1;
  double delta = sk/kmax;
  saidx64_t sumlcpstat = 0;
  for(saidx64_t k=kmax-1;k>0;k--) {
    sumlcpstat += lcpstats[k];
    sk = n-k+1 - sumlcpstat;
    assert(sk>0);
    double tmp = sk/k;
    if(tmp>delta) {
      delta=tmp; kmax=k;
      if(Verbose) fprintf(stderr,"  k=%ld, S(k)=%ld, delta=%g\n",kmax,(long)sk,delta);
    }
  }
  finish = clock();
  fprintf(stderr, "Total elapsed time: %.4f sec\n", (double)(finish - start0) / (double)CLOCKS_PER_SEC);  
  // --- report delta 
  fprintf(stdout,"=== file: %s   delta: %lf [%ld]\n",fname,delta,kmax);
  
  /* Deallocate memory. */
  free(T);
  free(SA);
  return 0;
}



/* ***********************************************************************
   WARNING sa, lcp, rank_next are 1 based: sa[n] is ok sa[0] is segfault  
   space economical computation of the lcp array
   This procedure is similar to the algorithm of kasai et al.  but we compute 
   the rank of i using the rank_next map (stored in the rn array) instead of
   the rank array. The advantage is that as soon as we have read rn[k] that
   position is no longer needed and we can use it to store the lcp.
   Thus, rn[] and lcp[] share the same memory and the overall space
   requirement of the procedure is 9n instead of 13n.
    input
      t[0,n-1] input text 
      sa[1,n] suffix array in the lower 32bit of each 64bit int entries 
    return
      nothing
      the lcp[2,n] entries are stored in the high 32 bit of each sa[] entry
      lcp[i] is the lcp between t[sa[i-1]...] and t[sa[i]...] 
    no additional space in addition to t[] and sa[] is used 
   *********************************************************************** */
static void sa2lcp_9n(sauchar_t *t, saidx64_t n, saidx64_t *sa)
{
  saidx64_t i,h,j,k,nextk=-1;
  saidx64_t *rn;
  saidx64_t maxlcp = 0;

  // rn and sa are pointers to the same array
  rn =  sa;
  // compute rank_next map
  k = sa2ranknext(t, n, sa); // k is the rank of t[0...]
  if(Verbose>1)
    fprintf(stderr,"ranknext computation done\n");
  for(h=i=0; i<n; i++,k=nextk) {
    assert(k>0 && i==(sa[k]&MASK32)); 
    nextk=(rn[k]>>32)&MASK32;          // read nextk before it is overwritten
    if(k>1) {
      // compute lcp between suffixes of rank k and k-1 (recall i==sa[k])
      j = sa[k-1] & MASK32;
      while(i+h<n && j+h<n && t[i+h]==t[j+h])
        h++;
      // check lcp fits in 32 bit   
      if(h>MASK32) {fprintf(stderr,">32 bits lcp value: %ld. Exiting\n",(long) h); exit(EXIT_FAILURE);}
      if(h>maxlcp) maxlcp=h;           // compute also maximum LCP 
      sa[k]=(h<<32)|(sa[k]&MASK32);    // was lcp[k]=h   h is the lcp, write it to lcp[k];  
    }
    if(h>0) h--;
  }
  assert(nextk==0);        // we have reached the last suffix s[n-1]
  sa[1] = (maxlcp<<32)|(sa[1]&MASK32); // write maxlcp in lcp[1]
  return;
}


/* *******************************************************************
   WARNING sa, lcp, rank_next are 1 based 
   input 
     n size of text
     t[0,n-1] input text
     sa[1,n] suffix array (low 32 bits)
  output
    rank_next[1,n] is written in the high 32 bits of sa[1,n] 
    note: ranks are in the range [1,n]
  return 
    r0 = rank of t[0,n-1] (it is not a rank_next value), 
    sa[r0]=0 (this is an invalid  rank)
  space: inplace except for the occ and count arrays (occ can be avoided)
  ******************************************************************* */
saidx64_t sa2ranknext(sauchar_t *t, saidx64_t n, saidx64_t *sa)
{
  saidx64_t i,j,sai,c,r0=0;
  saidx64_t count[ALPHABET_SIZE], occ[ALPHABET_SIZE];

  // compute # occurrences of each char 
  for(i=0;i<ALPHABET_SIZE;i++) occ[i]=0;
  for(i=0;i<n;i++) occ[t[i]]++;
  // --- occ -> count
  count[0]=0;
  for(i=1;i<ALPHABET_SIZE;i++) {
    count[i] = count[i-1] + occ[i-1];
  }
  
  // --- sa+t -> rank_next
  j = ++count[t[n-1]];       // this is bwt[0]
  assert(j>0 && j<=n);
  sa[j] |= 0; // here there was rank_next[j]=0, note 0 is an invalid rank value;
  for(i=1;i<=n;i++) {
    sai = (sa[i] & MASK32);
    assert(sai>=0 && sai<n);
    if(sai == 0)
      r0 = i;
    else {
      c = t[sai-1];
      j = ++count[c];
      assert(j>0 && j<=n);
      sa[j] |= (i<<32); // rank_next[j] = i
    }
  }
  assert(r0>0);
  return r0;
}


/* *******************************************************************
   check the correctness of a lcp array with a (very slow) 
   character by character comparison of consecutive suffixes
   input
     t[0,n-1]   input text 
     sa[0,n-1]  sa (low32) and lcp (high32) arrays
   ******************************************************************* */ 
static void check_lcp_array(sauchar_t *t, saidx64_t n, saidx64_t *sa)
{
  saidx64_t i,j,lcpi,k,h;

  if(Verbose)
    fprintf(stderr,"Checking lcp values (very slow)...\n");
  for(i=1;i<n;i++) {
    j = (sa[i-1] & MASK32); k=(sa[i] & MASK32);
    for(h=0;j+h<n && k+h<n;h++)
      if(t[j+h]!=t[k+h]) break;
    lcpi = (sa[i]>>32) & MASK32;
    if(lcpi!=h) {
      fprintf(stderr,"FATAL ERROR! Incorrect LCP value: lcp[%"PRIdSAIDX64_T
                     "]=%"PRIdSAIDX64_T"!=%"PRIdSAIDX64_T"\n",
        i,lcpi,h);
      exit(EXIT_FAILURE);
    }
  }
  if(Verbose)
    fprintf(stderr,"LCP array OK!\n");
}


static void
print_help(const char *progname, int status) {
  fprintf(stderr, "usage: %s filename\n", progname);
  fprintf(stderr, "\t-c check sa\n");
  fprintf(stderr, "\t-k check lcp (very slow!)\n");
  fprintf(stderr, "\t-v verbose output\n\n");
  exit(status);
}


